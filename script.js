    // Init Social Share Kit
    SocialShareKit.init({
        'url': 'http://socialsharekit.com',
        // 'url': 'http://geeksider.net',
        'twitter': {
            'title': 'Fuuuuuuuuuuck!!',
            'text': 'mother fuzzer text',
            'via': 'atnovember'
        }
    });

    $(function () {

        // Just to disable href for other example icons
        $('.ssk').on('click', function (e) {
            e.preventDefault();
        });

        // Navigation collapse on click
        $('.navbar-collapse ul li a:not(.dropdown-toggle)').bind('click', function () {
            $('.navbar-toggle:visible').click();
        });

        // Email protection
        $('.author-email').each(function () {
            var a = '@', em = 'support' + a + 'social' + 'sharekit' + '.com', t = $(this);
            t.attr('href', 'mai' + 'lt' + 'o' + ':' + em);
            !t.text() && t.text(em);
        });

        // Sticky icons changer
        $('.sticky-changer').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            $('.ssk-sticky').removeClass($link.parent().children().map(function () {
                return $(this).data('cls');
            }).get().join(' ')).addClass($link.data('cls'));
            $link.parent().find('.active').removeClass('active');
            $link.addClass('active').blur();
        });
    });



/*

SocialShareKit.init({
	    selector: '.ssk',
	    url: 'http://my-url',
	    title: 'A long time ago, in a galaxy far, far away...',
	    text: 'When I was too much younger little html-junior... #starwars ',
	    description: 'When I was too much younger little html-junior... #starwars ',
	    image: "/img/star-wars-logo.png"*/

	    /*vk: {
	        url: 'http://url-for-vk',
	        text: 'Share text for vk',
	        title: 'Хороший сайт',
  			description: 'Это мой собственный сайт, я его очень долго делал',
  			image: 'http://mysite.com/mypic.jpg',
  			noparse: true
	        via: 'vk-screen-name',

	    }*/
	    /*via: 'http://twitter/atnovember'*/

	   /* twitter: {
	        url: 'http://url-for-twitter',
	        text: 'Share text for twitter',
	        via: 'twitter-screen-name',
	    }*/
	// });